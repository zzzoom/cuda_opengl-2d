CUDASDK=/opt/cudasdk/C/common/inc
CXX=nvcc
CXXFLAGS=-g -G
CPPFLAGS=-I$(CUDASDK)

SOURCES=$(shell echo *.cu)
OBJECTS=$(SOURCES:%.cu=%.o)

all: cudagl2d.o

%.o: %.cu
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) -c $< -o $@

.PHONY: clean

clean:
	rm -f $(OBJECTS)
