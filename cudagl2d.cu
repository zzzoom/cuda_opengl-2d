/*
 Copyright (c) 2012 Carlos Bederián <bc@famaf.unc.edu.ar>
 License: http://www.gnu.org/licenses/gpl-2.0.html
*/

#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/freeglut.h>

#include <cuda.h>
#include <cuda_gl_interop.h>
#include <cutil_inline.h>
#include <stdio.h>

#include "cudagl2d.h"

static uint2 window_size;
static GLuint gl_tex;
static GLuint gl_pbo;
static cudaGraphicsResource_t cuda_pbo_resource;


uchar4 * cudagl2d_get_pixelbuffer(void) {
    uchar4 * ptr;
    size_t len;
    cutilSafeCall(cudaGraphicsMapResources(1, &cuda_pbo_resource));
    cutilSafeCall(cudaGraphicsResourceGetMappedPointer(reinterpret_cast<void**>(&ptr), &len, cuda_pbo_resource));
    return ptr;
}


void cudagl2d_draw(void) {

    // unmap array before drawing
    cutilSafeCall(cudaGraphicsUnmapResources(1, &cuda_pbo_resource));

    // clear the screen
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // enable texturing and select texture gl_tex
    glEnable(GL_TEXTURE_2D);
    // glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);
    glBindTexture(GL_TEXTURE_2D, gl_tex);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, gl_pbo);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, window_size.x, window_size.y, GL_BGRA, GL_UNSIGNED_BYTE, NULL);

    // start a quad
    glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3f(0.0f, 0.0f, 0.0f);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(0.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3f(1.0f, 1.0f, 0.0f);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3f(1.0f, 0.0f, 0.0f);
    glEnd();

    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glBindTexture(GL_TEXTURE_2D, 0);

    // bring backbuffer to front
    glutSwapBuffers();
    // tag window as dirty
    glutPostRedisplay();
    // tell freeglut/openglut to do event processing just once
    glutMainLoopEvent();
}


static void cudagl2d_pbo_init(uint w, uint h, GLuint * pbo_p, cudaGraphicsResource_t * resource_p) {
    size_t size = w * h * 4;
    glGenBuffers(1, pbo_p);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, *pbo_p);
    glBufferData(GL_PIXEL_UNPACK_BUFFER, size, NULL, GL_DYNAMIC_DRAW);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    cutilSafeCall(cudaGraphicsGLRegisterBuffer(resource_p, *pbo_p, cudaGraphicsRegisterFlagsWriteDiscard));
}


static void cudagl2d_tex_init(uint w, uint h, GLuint * tex_p) {
    // enable texturing
    glEnable(GL_TEXTURE_2D);

    // generate named texture
    glGenTextures(1, tex_p);

    // bind the texture and set it up
    glBindTexture(GL_TEXTURE_2D, *tex_p);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_REPEAT);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_BGRA, GL_UNSIGNED_BYTE, NULL);
    glBindTexture(GL_TEXTURE_2D, 0);
}


void cudagl2d_init(int argc, char **argv, unsigned int device, unsigned int w, unsigned int h) {

    // Get a cuda context with gl support
    cutilSafeCall(cudaGLSetGLDevice(device));

    // Pass runtime parameters to glut
    glutInit(&argc, argv);

    // Create a 32bpp, double buffered window
    glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
    glutInitWindowSize(w, h);
    glutCreateWindow("CUDA OpenGL");

    // TODO: this isnt the real window size
    //       need to register a callback and ask for a resize
    window_size.x = w;
    window_size.y = h;

    // Load extensions
    GLenum err = glewInit();
    if (GLEW_OK != err) {
        fprintf(stderr, "Unable to init GLEW: %s\n", glewGetErrorString(err));
        exit(1);
    }

    // check for pbo support
    if (! GLEW_ARB_pixel_buffer_object) {
        fprintf(stderr, "OpenGL platform doesn't support pixel buffer objects\n");
        exit(1);
    }

    // Set up OpenGL projection and switch to model view for the rest
    glViewport(0, 0, w, h);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glDisable(GL_DEPTH_TEST); // 2D

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0.0f, 1.0f, 0.0f, 1.0f, -1.0f, 1.0f);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    cudagl2d_tex_init(w, h, &gl_tex);
    cudagl2d_pbo_init(w, h, &gl_pbo, &cuda_pbo_resource);

    // FIXME: set atexit or force the user to do explicit cleanup?
    atexit(cudagl2d_cleanup);
}


void cudagl2d_cleanup() {
    cudaGraphicsUnregisterResource(cuda_pbo_resource);
    glDeleteTextures(1, &gl_tex);
    glDeleteBuffers(1, &gl_pbo);
}
