/*
 Copyright (c) 2012 Carlos Bederián <bc@famaf.unc.edu.ar>
 License: http://www.gnu.org/licenses/gpl-2.0.html
*/

#pragma once

#include <cuda.h>

#ifdef __cplusplus
extern "C" {
#endif

// Sets up a CUDA context on CUDA device #device with an
// OpenGL context on the same device and a w x h window.
// OpenGL runtime parameters are forwarded to GLUT.
void cudagl2d_init(int argc, char ** argv, unsigned int device, unsigned int w, unsigned int h);

// Maps a row-major w*h pixel buffer of BGRA uchar4s to CUDA and gives a pointer to it.
// NOTE: This must be remapped after each call to cudagl_draw().
uchar4 * cudagl2d_get_pixelbuffer(void);

// Unmaps the pixel buffer and draws its contents on the window
void cudagl2d_draw();

// Cleans up
void cudagl2d_cleanup();

#ifdef __cplusplus
}
#endif
